mv biologbook.db biologbook_$(date +%y%m%d-%H%M%S).db
mv modelRDS modelRDS_$(date +%y%m%d-%H%M%S)

ssh -i ~/.ssh/biologbook.pem ec2-user@3.16.191.82 'cd src;tar -czvf back_up_models.tar.gz modelRDS/ biologbook.db'
scp -i ~/.ssh/biologbook.pem ec2-user@3.16.191.82:src/back_up_models.tar.gz .
ssh -i ~/.ssh/biologbook.pem ec2-user@3.16.191.82 'cd src;rm -f back_up_models.tar.gz'
tar xvfz back_up_models.tar.gz
rm -f xvfz back_up_models.tar.gz