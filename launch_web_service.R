rlib = paste(R.home(),"library",sep="/");
.libPaths(rlib)
packages_to_install = c("devtools","withr","logging");
git_hub_to_install = c("jakezblb/blbpackage/a4a0407bfd3885a2248ba5c8ec508aba6db689b6")
to_install<-packages_to_install %in% rownames(installed.packages(rlib))
print(packages_to_install[!to_install])
if (FALSE %in% to_install){
  install.packages(packages_to_install[!to_install],dependencies=TRUE,repos='http://cran.us.r-project.org',lib=rlib)
}


lapply(packages_to_install, require, character.only = TRUE, quietly=TRUE,lib.loc=rlib);


with_libpaths(new = rlib, install_github('jakezblb/blbpackage',auth_token = 'a4a0407bfd3885a2248ba5c8ec508aba6db689b6'))

#detach("package:biologbook", unload=TRUE)
library("biologbook",lib.loc = rlib );

toinstall = c("plumber");


 debug = FALSE;

debugOptimization = FALSE;
if(isTRUE(debug)){
   basicConfig(level="debug")
 }else {
   basicConfig()
 }
 

installRPackages(toinstall);
libraryPackages(toinstall);
print("launching webservice")
r <- plumb("Controller.R");r$run(host="0.0.0.0",port=8000)
