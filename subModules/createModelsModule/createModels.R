
createModels <- function(input, output, session) {
  ns =session$ns
  print(ns(""));
  paramYs=getQuantitativeParameters()$PARAM;
  
  paramXs=getParametersTypeParameterNonEmpty()$PARAM
  
  paramWithDatas = unique(db$parametre);
  paramYs=sort(paramYs[paramYs%in%paramWithDatas]);
  paramXs=sort(paramXs[paramXs%in%paramWithDatas]);
  paramXs=c("*",paramXs);
  paramXs=c("",paramXs);
  
  
  output$aExpliquerSelect=renderUI({
    
    selectInput(
      ns("aExpliquerSelect"),
      label = "Parametre a expliquer",
      choices = paramYs,
      selected = "")
  })
  
  output$explicatifSelect1=renderUI({
    ns =session$ns
    selectInput(
      ns("explicatifSelect1"),
      label = "Parametre explicatif 1",
      choices = paramXs
    )
  })
  
  output$explicatifSelect2=renderUI({
    ns =session$ns
    selectInput(
      ns("explicatifSelect2"),
      label = "Parametre explicatif 2",
      choices = paramXs
    )
  })
  output$explicatifSelect3=renderUI({
    ns =session$ns
    selectInput(
      ns("explicatifSelect3"),
      label = "Parametre explicatif 3",
      choices = paramXs
    )
  })
  
  #oberse selection of parameters 
  observeEvent( input$aExpliquerSelect, {
    logjs(paste("input$aExpliquerSelect changed",input$aExpliquerSelect));
    paramX = paramXs[paramXs!=input$aExpliquerSelect]
    paramX=c("*",paramX);
    paramX=c("",paramX);
    updateSelectInput(session, "explicatifSelect1",
                      choices = paramX
    )
    filteredDB = refactorDataOnParameterNames(db,c(input$aExpliquerSelect,input$explicatifSelect1,input$explicatifSelect2,input$explicatifSelect3))
    output$filteredDB = renderDT(filteredDB,rownames = FALSE,filter = list(position = 'top'),
                                 options = list(
                                   search = list(regex = TRUE, caseInsensitive = TRUE),
                                   pageLength = 50))
    
  })
  observeEvent( input$explicatifSelect1, {
    logjs(paste("input$explicatifSelect1 changed",input$explicatifSelect1));
    paramX = paramXs[paramXs!=input$explicatifSelect1]
    paramX=c("*",paramX);
    paramX=c("",paramX);
    updateSelectInput(session, "explicatifSelect2",
                      choices = paramX
    )
    filteredDB = refactorDataOnParameterNames(db,c(input$aExpliquerSelect,input$explicatifSelect1,input$explicatifSelect2,input$explicatifSelect3))
    output$filteredDB = renderDT(filteredDB,rownames = FALSE,filter = list(position = 'top'),
                                 options = list(
                                   search = list(regex = TRUE, caseInsensitive = TRUE),
                                   pageLength = 50))
    
  })
  observeEvent( input$explicatifSelect2, {
    logjs(paste("input$explicatifSelect2 changed",input$explicatifSelect2));
    paramX = paramXs[paramXs!=input$explicatifSelect2]
    paramX = paramX[paramX!=input$explicatifSelect1]
    paramX=c("*",paramX);
    paramX=c("",paramX);
    updateSelectInput(session, "explicatifSelect3",
                      choices = paramX
    )
    filteredDB = refactorDataOnParameterNames(db,c(input$aExpliquerSelect,input$explicatifSelect1,input$explicatifSelect2,input$explicatifSelect3))
    output$filteredDB = renderDT(filteredDB,rownames = FALSE,filter = list(position = 'top'),
                                 options = list(
                                   search = list(regex = TRUE, caseInsensitive = TRUE),
                                   pageLength = 50))
  })
  
  
  observeEvent(input$launchStatButton, {
    launch_stat();
  })
  
  observeEvent(input$startStatistical, {
    logjs(paste("input$launchStatButton pressed with parameters",input$aExpliquerSelect,input$explicatifSelect1,input$explicatifSelect2,input$explicatifSelect3));
    #disable button until calculation is done
    #toggleState(session$ns)
    aExpliquerSelect = input$aExpliquerSelect
    explicatifSelect1 = input$explicatifSelect1
    explicatifSelect2 = input$explicatifSelect2
    explicatifSelect3 = input$explicatifSelect3
    explicatives = explicatifSelect1;
    if(input$aExpliquerSelect=="" || input$explicatifSelect1==""){
      logjs("data not valid")
      return()
    }
    if(explicatifSelect2!=""){
      explicatives=c(explicatives,explicatifSelect2)
    }
    if(explicatifSelect3!=""){
      explicatives=c(explicatives,explicatifSelect3)
    }
    explicatives = sort(explicatives)
    explicatives = sort(explicatives)
    explicatives = explicatives[explicatives!=""]
    
    if("*"%in%explicatives){
      res = createAndComputeModelLoop(aExpliquerSelect,explicatives)
    }
    else{
      res = createAndComputeModel(aExpliquerSelect,explicatives)
    }
    
    #x$refModel = getAllFromTable("REF_MODEL")
    # refModelMV = modelsMV(x$refModel)
    # output$model <- setModelDTOutput(refModelMV,x$rowSelected,x$searches,x$searchBar)
    #disable button until calculation is done
    #toggleState(session$ns)
  })
  
  
  
  
  
  
}


createAndComputeModel<-function(aExpliquer,explicatives){
  functionName<-match.call()[[1]]
  step<-"Start"
  tryCatch({
    refModel=getAllFromTable("REF_MODEL")
    db=getAllFromAnonymeTable()
    if(is.null(getDefaultReactiveDomain()) ){
      uiActionsEnabled = FALSE
    } else {
      print("not in shiny")
      uiActionsEnabled = TRUE
    }
    
    formulaString = paste(aExpliquer,"~",paste(explicatives,collapse="+"),sep="")
    if(isTRUE(uiActionsEnabled)){logjs(formulaString)};
    #check if model already exists
    models=refModel[refModel$NAME==formulaString,];
    if(dim(models)[1]>=1){
      model = readRDS(models$FILE)
      model$name = formulaString;
      if(isTRUE(uiActionsEnabled)){
        showNotification(paste(formulaString,"existe deja"),type="message",duration=3)
        # output$modelTextComputed = renderPrint(model$aovSumModel);
        # output$plotModelComputed = renderPlot(plotModel(model))
      }
    }
    else{
      linearModel(db,aExpliquer,explicatives)
      refModel=getAllFromTable("REF_MODEL")
      models=refModel[refModel$NAME==formulaString,];
      if(dim(models)[1]>=1){
        model = readRDS(models$FILE)
        model$name = formulaString;
        order = list(list('2','desc'))
        if(isTRUE(uiActionsEnabled)){
          showNotification(paste(formulaString,"a ete calcule"),type="message",duration=3)
          # output$modelTextComputed = renderPrint(model$aovSumModel);
          # output$plotModelComputed = renderPlot(plotModel(model))
          # selected = x$rowSelected;
          # searches = x$searches
          # searchBar = x$searchBar;
          # x$refModel = getAllFromTable("REF_MODEL")
          # refModelMV = modelsMV(x$refModel)
          # output$model <- setModelDTOutput(refModelMV,x$rowSelected,x$searches,x$searchBar)
       
        }
      }
      else {
        if(isTRUE(uiActionsEnabled)){
          showNotification(paste("Les donnees ne permettent de calculer le modele",formulaString),type="warning",duration=3)
        }
        
      }
      
      
    }
    
  }, error = function(err) onError(err,functionName,step ))
}

updateOutputShiny<-function()

createAndComputeModelHandler<-function(aExpliquer,explicatives){
  functionName<-match.call()[[1]]
  step<-"Start"
  tryCatch({
    
    explicatives = sort(explicatives)
    explicatives = explicatives[explicatives!=""]
    
    if("*"%in%explicatives){
      createAndComputeModelLoop(aExpliquer,explicatives)
    }
    else{
      createAndComputeModel(aExpliquer,explicatives)
    }
    
  }, error = function(err) onError(err,functionName,step ))
}


#' createAndComputeModelLoop
#'
#' @param aExpliquer 
#' @param explicatives 
#'
#' @return
#' @export
#' @importFrom shiny getDefaultReactiveDomain showNotification
#' @examples
createAndComputeModelLoop<-function(aExpliquer,explicatives){
  functionName<-match.call()[[1]]
  step<-"Start"
  tryCatch({
    
    refModel=getAllFromTable("REF_MODEL")
    db=getAllFromAnonymeTable()
    explicatives = explicatives[explicatives!="*"]
    paramX = getParametersTypeParameterNonEmpty()$PARAM
    paramWithDatas = unique(db$parametre);
    paramX=paramX[paramX%in%paramWithDatas];
    paramX=paramX[!paramX%in%aExpliquer];
    parametersLooped = paramX[!paramX%in%explicatives]
    
    for (param in parametersLooped){
      explicativesLoop = c(explicatives,param)
      createAndComputeModel(aExpliquer,explicativesLoop)
    }
    
    
    
    
  }, error = function(err) onError(err,functionName,step ))
}



