

#!/bin/bash
if [ -z "$1" ]
  then
    echo "No port supplied"
elif [ -z "$2" ]
	then
		echo "No action supplied"
else
	getPidFromPort () {
		local port=$1
		local pid=`sudo netstat -plten | grep 0.0.0.0:$port | rev | cut -d'/' -f2 | cut -d' ' -f1 |rev`
		echo "$pid"
	}
	
	startService () {
		pid=$(getPidFromPort $1)
		if [ ! -z "$pid" ]
		then
			sudo kill -9 $pid
			if [ $? = 0 ]
				then
					echo "process listening on $1 stopped"
					
			fi
		fi
			
			
		if [ $1 = 6333 ]
			then
				cd src
				path="../log/blbshiny$(date +%Y%m%d%H%M%S).log"
				nohup Rscript blbshiny.R &>$path&
				cd ..
				echo "BLB R&D launched on port 6333"
		elif [ $1 = 8000 ]
			then
				cd src
				path="../log/blbapi$(date +%Y%m%d%H%M%S).log"
				nohup Rscript launch_web_service.R &>$path&
				cd ..
				echo "BLB api launched on port 8000"
				
			
		fi
	}
	stopService () {
		local port=$1
		local pid=`sudo netstat -plten | grep 0.0.0.0:$port | rev | cut -d'/' -f2 | cut -d' ' -f1 |rev`
		if [ ! -z "$pid" ]
		then
			sudo kill -9 $pid
			if [ $? = 0 ]
				then
					echo "process listening on $1 stopped"
					cd src
			fi
		else
			echo "No process listening on port $1"
		fi
	}
	if [ $2 = "start" ]
		then
			startService $1
	elif [ $2 = "restart" ]
		then	
			startService $1
	elif [ $2 = "stop" ]
		then
			stopService $1
	else
		echo "$2 is not a known action"
	fi
	
	
fi